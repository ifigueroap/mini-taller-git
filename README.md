﻿# Mini Taller de Git

Hola!
Bienvenido al mini-taller de Git. Para que funcione bien desde la red PUCV utiliza la dirección con protocolo HTTPS.

## Tarea 1

Tu primera tarea es clonar este repositorio.

## Tarea 2

Tu segunda tarea es agregar tu nombre a la lista de participantes. Además de tu nombre, agrega una cita, cuento, poema o fragmento de un libro que te guste. Estos cambios solo estarán disponibles en tu repositorio local.

## Tarea 3

Una vez que hayas editado el archivo, registra tus cambios *haciendo un commit*. Observa ahora la historia de tu repositorio local.

## Tarea 4

Para compartir tus cambios con el mundo, *haz un push* del repositorio. Para esto probablemente debas primero actualizarte, *haciendo un pull*.

## Tarea 5

Crea una carpeta con tu nombre. Dentro de esa carpeta, escribe un programa *Hola Mundo* en el lenguaje que quieras. Luego haz commit y push.

## Tarea 6

Actualiza tu copia de trabajando haciendo un pull. Ahora modifica el código de algún compañero. Nuevamente haz commit y push.

## Tarea 7: Deshacer cambios ***antes de hacer commit***.

Abre el archivo 'pokemones.html' y borra todo su contenido. Ups!!!!
Para ***resetear el archivo al contenido que tenía en el último pull*** utiliza la opción *reset* de SourceTree. O bien, por línea de comandos ejecuta "git checkout pokemones.html".

## Tarea 8: Deshacer cambios del último commit (a.k.a. "la embarré")

Actualiza el repositorio haciendo pull. Ahora modifica cualquier cantidad de archivos, con cambios que dejan todo mal. ***Haz un commit, pero no un push***.
En tu historia local el último commit tiene cambios que desearías no haber hecho nunca.

Para crear ***un nuevo set de cambios que revierten ese commit*** utiliza la opción *Reset to Commit*. Puedes elegir cualquier punto en la historia, y crear cambios que deshacen lo contribuído en ese punto específico.

## Tarea 9

Cambiale el nombre al archivo que hiciste en la Tarea 5. Para que git sepa que estás renombrando utiliza la opción *Move* en SourceTree, o ejecuta el comando *git mv*. Luego haz commit y push.

## Tarea 10

Borra el archivo que creaste en la Tarea 5, y que renombraste recien en la Tarea 9. Para eso utiliza la opción *Remove* en SourceTree, o ejecuta el comando *git rm*. Luego haz commit y push.

## Lista de Participantes

- Ismael Figueroa, el profe. "En teoría no hay diferencia entre la teoría y la práctica. Pero en la práctica las diferencias son muchas".

- Franco Valerio, un alumno más. "You are a wizard Harry!". 

- Patricio López, unknown. "Quiero que se repita la ocasión. Quiero que se repitan tus movimientos."

- Oscar Arancibia : "Tengo una amigo LUIGGI :)".

- Danilo Tapia, el alumno. "Los milagros no existen. Solo existe lo inevitable, la casualidad y el resultado de tus propias acciones".

- Fernando Pérez, alumno. "That's my secret, cap. I'm always angry"

- Fabián Carvajal, alumno. "my precious!!!!"

- Cristian Villalobos, cristian. "holi"

- Cristian Villalobos, cristian. "oscargay"

